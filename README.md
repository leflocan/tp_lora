# Projet IoT

Ce projet est basé sur trois tutorials de RandomNerdtutorial.

## Mail
Cette partie est basé sur le projet suivant : [lien](https://randomnerdtutorials.com/esp32-cam-send-photos-email/) \
Nous avons dans un premier temps mis en place un server SMTP avec Postfix sur une machine perso. \
Puis, grâce au package ESP32_MailClient, nous avons envoyé des mails à une adresse mail. \
Enfin, nous avons ajouté un fichier .txt à l'ESP32 (pio run -t uploadfs) pour l'envoyer comme pièce jointe.

## Paxcounter
Nous avons dans un premier temps pris le [code](https://github.com/cyberman54/ESP32-Paxcounter) et configuré les fichiers. \
Puis, nous avons enregistré un nouvel appareil sur le server loraserver.tetaneutral.net \
Il ne restait plus qu'à configurer DEVEUI, APPEUI et APPKEY pour permettre les communications entre l'ESP32 et le server. \
Nous avons ensuite regardé sur l'interface chirpstack les datas envoyé par l'ESP ([application IbdIoT-2](https://loraserver.tetaneutral.net/#/organizations/5/applications/16/devices/ade50bd219074bc6)). Selon le git, nous pouvons en déduire facilement le nombre d'appareils WiFi et Bluetooth détectés. Ces données sont encodée en base64, on peut le décoder facilement grâce à [ce site.](https://cryptii.com/pipes/base64-to-hex)

```
byte 1-2:	Number of unique devices, seen on Wifi 
byte 3-4:	Number of unique devices, seen on Bluetooth 
```

Malheureusement, le nombre d'appareils WiFi détecté correspond au nombre d'AP dans la zone. \
Ceci n'est donc pas représentatif du nombre de personnes présente. 


## Web page
Ce dernier projet a pour but d'afficher sur une page web le relevé de Paxcounter. \
il est basé sur le code suivant : [lien](https://randomnerdtutorials.com/esp32-dht11-dht22-temperature-humidity-web-server-arduino-ide/). \
Une simple page web prend en paramètre deux entiers et les affiche. \
<img src="./Images/Serverweb.png" width="300px"/>


## Fusion
<img src="./Images/fusion.jpeg" width="200px"/> \
Nous avons ensuite essayé de fusionner Paxcounter et la page Web. \
Pour cela, nous avons ajouté les fichiers main_web.cpp et main_web.h \
Malheureusement nous avons rencontré de nombreux problèmes avec le package ESPAsyncWebServer. En effet, time.h est défini à la fois en c et pour windows ce qui posait des problèmes de double écriture d'entête. \
Nous avons toutefois réussi à résoudre ce problème en passant sous Ubuntu. 

Toutefois, nous n'avons pas réussi à finaliser la fusion car Paxcounter effectue une boucle dans la méthode setup() et non dans le loop(). Nous n'avons donc pas réussi à insérer notre boucle de réponse HTTP. 

## Finalité
Le but ultime du projet était de fusionner ces trois codes et ainsi avoir un rapport journalier de Paxcounter envoyé par mail et une page web affichant en direct le nombre de personnes à un endroit. \
Dans l'idéal, l'ESP32 enverrait ses données avec une méthode PUT à un server web qui pourrait gérer un ensemble de capteurs réparti dans l'école. Ce même serveur pourrait envoyer un mail journalier, ce qui allègerai le travail des ESP.

### Authors
Campion Nathan \
Dambrine Vincent \
Le Floch Antonin \
Sigismeau Rémi 

